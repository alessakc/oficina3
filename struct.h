#define TAM 4

	typedef struct wav_t {

		struct c1 {
			char chunkID[TAM];   //"RIFF"
			int chunksize;       // tamanho do arquivo
			char format[TAM];    // "WAVE"
		} c1; 

		struct c2 {
			char schunk1[TAM];   // "fmt "
			int schunk1size;     // tamanho deste chunk
		} c2;

		struct c3 {
			short int audiof;    // codificacao utilizada
			short int n_canais;  // numero de canais de audio	
		} c3;

		struct c4 {
			int srate;           // taxa de amostragem
			int brate;           // taxa de bytes/s	
		} c4;

		struct c5 {
			short int balign;    // bytes por amostra
			short int bsample;   // bits por amostra	
		} c5;

		struct c6 {
			char schunk2[TAM];   // "data"
			int schunk2size;     // espaço ocupado pelas amostras
		} c6;

		struct c7 {
			int a_data;          // numero de amostras
			int16_t *amostras; // canais de amostras
		} c7;

	} wav_t;

	typedef enum {
		NORMAL,
		REVERSO
	} filtro;

