#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"


int confere_wav(wav_t *audio) {

	int i;
	char riff[4] = "RIFF";
	char wave[] = "WAVE";
	char fmt[] = "fmt ";
	char data[] = "data";

	for (i = 0; i < 4; i++)
		if (riff[i] != audio->c1.chunkID[i]) 
			return (0);

	for (i = 0; i < 4; i++)
		if (wave[i] != audio->c1.format[i]) 
			return (0);


	for (i = 0; i < 4; i++)
		if (fmt[i] != audio->c2.schunk1[i]) 
			return (0);


	for (i = 0; i < 4; i++)
		if (data[i] != audio->c6.schunk2[i]) 
			return (0);

	return (1);
}

void ler_arquivo(wav_t *audio, FILE *entrada) {

	/* k = numero total de canais */
	int k;
	int i, c;

	/* le as informacoes */
	fread (&audio->c1, 3, 4, entrada);
	fread (&audio->c2, 2, 4, entrada);
	fread (&audio->c3, 2, 2, entrada);
	fread (&audio->c4, 2, 4, entrada);
	fread (&audio->c5, 2, 2, entrada);
	fread (&audio->c6, 2, 4, entrada);

   	if (!confere_wav(audio)) {
		fprintf(stderr, "Nao eh arquivo wav.\n");
		exit (-1);
	} 

	/* o numero de canais de amostras é igual ao numero de 
	   bytes total das amostras divido pelo numero de bytes
	   de cada amostra                                      */
	audio->c7.a_data = (audio->c6.schunk2size/(audio->c5.balign));

	k = (audio->c7.a_data)*(audio->c3.n_canais) + 1;
	
	/* reserva memoria para as amostras */
	audio->c7.amostras = malloc (k * sizeof(int16_t));

	/* le as amostras */
	for (i = 0; i <= k; i++) {
		fread (&c, 1, 2, entrada);
		audio->c7.amostras[i] = c;	
	}
}


void opcao_entrada (int argc, char *argv[], FILE **entrada) {

	int i;
	/* confere se tem entrada por -i*/
	for (i = 0; i < argc; i++)
		if (strcmp(argv[i], "-i") == 0)
			if (((i+1) < argc) && ((i+1) != 76) && ((i+1) != 55)) 
				*entrada = fopen(argv[i+1], "rb");

}

void opcao_saida (int argc, char *argv[], FILE **saida) {

	int i;
	/* confere se tem entrada por -i*/
	for (i = 0; i < argc; i++)
		if (strcmp(argv[i], "-o") == 0)
			if ((i+1) < argc)
				*saida = fopen(argv[i+1], "w");

}

void libera_memoria (wav_t *audio) {

	free (audio->c7.amostras);
	audio->c7.amostras = NULL;
}