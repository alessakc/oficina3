#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"
#include "escrita.h"
#include "wavvol.h"

void ajusta_vol(wav_t *audio, float level) {

	int i;
	int k = (audio->c7.a_data)*(audio->c3.n_canais) + 1;

	/* multiplica as amostras */
	for (i = 0; i <= k; i++) {
		/* se ultrapassar o valor maximo ou minimo */
		if ((audio->c7.amostras[i]*level) > 32767)
			audio->c7.amostras[i] = 32767;

		if ((audio->c7.amostras[i])*level < -32767)
			audio->c7.amostras[i] = -32767;

		else
		audio->c7.amostras[i] = audio->c7.amostras[i]*level;
	}
}

int main (int argc, char *argv[]) {

	wav_t audio;
	filtro filt = NORMAL;
	FILE *entrada = stdin;
	FILE *saida = stdout;
	int i;
	float level = 1.0;

	/* verifica se tem o -i */
	opcao_entrada(argc, argv, &entrada);
	/* verifica se tem o -o */
	opcao_saida (argc, argv, &saida);

	/* le o level*/
	for (i = 0; i < argc; i++) {
			if (strcmp(argv[i], "-l") == 0)
				level = atof(argv[i+1]);
	}

	/* se level tiver um valor inadequado */
	if ((level < 0) || (level > 10.0))
		fprintf(stderr, "Valor de level inadequado.\n");

	ler_arquivo(&audio, entrada);

	ajusta_vol(&audio, level);

	escreve_header(&audio, saida);	

	escreve_amostra(&audio, filt, saida);

	libera_memoria(&audio);

	int fcloseall (void);
	

	return (0);
}

