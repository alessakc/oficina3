all: wavinfo wavrev wavvol wavecho wavautovol wavwide wavcat

clean:
	-rm *~ *.o

purge:
	-rm wavinfo wavrev wavvol wavautovol wavwide wavcat wavecho wavmix

wavinfo: wavinfo.c wavinfo.h leitura.c leitura.h
	gcc wavinfo.c leitura.c -o wavinfo -Wall

wavrev: wavrev.c leitura.c leitura.h escrita.c escrita.h
	gcc wavrev.c leitura.c escrita.c -o wavrev -Wall

wavvol: wavvol.c wavvol.h leitura.c leitura.h escrita.c escrita.h
	gcc wavvol.c leitura.c escrita.c -o wavvol -Wall

wavautovol: wavautovol.c wavautovol.h leitura.c leitura.h escrita.c escrita.h
	gcc wavautovol.c leitura.c escrita.c -o wavautovol -Wall

wavecho: wavecho.c wavecho.h leitura.c leitura.h escrita.c escrita.h
	gcc wavecho.c leitura.c escrita.c -o wavecho -Wall

wavwide: wavwide.c wavwide.h leitura.c leitura.h escrita.c escrita.h
	gcc wavwide.c leitura.c escrita.c -o wavwide -Wall

wavcat: wavcat.c leitura.c leitura.h escrita.c escrita.h
	gcc wavcat.c leitura.c escrita.c -o wavcat -Wall

wavmix: wavmix.c leitura.c leitura.h escrita.c escrita.h 
	gcc wavmix.c leitura.c escrita.c -o wavmix -Wall