#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"
#include "escrita.h"

int main (int argc, char *argv[]) {

	wav_t audio;
	filtro filt = REVERSO;
	FILE *entrada = stdin;
	FILE *saida = stdout;
	
	/* verifica se tem o -i */
	opcao_entrada(argc, argv, &entrada);
	/* verifica se tem o -o */
	opcao_saida (argc, argv, &saida);

	ler_arquivo(&audio, entrada);

	escreve_header(&audio, saida);	

	escreve_amostra(&audio, filt, saida);

	libera_memoria(&audio);

	int fcloseall (void);
	
	return (0);
}