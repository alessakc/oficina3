#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"
#include "escrita.h"
#include "wavautovol.h"


int acha_maior (wav_t *audio) {
	int i, maior;
	int k = (audio->c7.a_data)*(audio->c3.n_canais) + 1;

	maior = 0;
	for (i = 0; i <= k; i++) {
		if (audio->c7.amostras[maior] < audio->c7.amostras[i])
			maior = i;
	}

	return (maior);
}

void ajuste(wav_t *audio) {

	int i, maior;

	/* n de amostras multiplicado pelo n de canais */
	int k = (audio->c7.a_data)*(audio->c3.n_canais) + 1;

	/* encontra o maior valor de amostra */
	maior = acha_maior(audio);

	/* multiplica as amostras */
	for (i = 0; i <= k; i++) {
		audio->c7.amostras[i] = (audio->c7.amostras[i]*32767/audio->c7.amostras[maior]);
	}
}

int main (int argc, char *argv[]) {

	wav_t audio;
	filtro filt = NORMAL;
	FILE *entrada = stdin;
	FILE *saida = stdout;

	/* verifica se tem o -i */
	opcao_entrada(argc, argv, &entrada);
	/* verifica se tem o -o */
	opcao_saida (argc, argv, &saida);

	ler_arquivo(&audio, entrada);

	ajuste(&audio);

	escreve_header(&audio, saida);	

	escreve_amostra(&audio, filt, saida);

	libera_memoria(&audio);

	int fcloseall (void);

	return (0);
}