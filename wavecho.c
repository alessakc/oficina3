#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"
#include "escrita.h"


void echo (wav_t *audio, float aten, int delay) {

	int i, t, e;

	/* total de amostras */
	t = (audio->c7.a_data)*(audio->c3.n_canais) + 1;

	/* duracao do eco */
	e = audio->c4.brate * (delay/1000);

	/* amostras recebem o echo */
	for (i = audio->c4.brate*delay/1000; i < t; i++) {
		audio->c7.amostras[i] +=  audio->c7.amostras[i-e]*aten;
	}

}

int main (int argc, char *argv[]) {

	wav_t audio;
	filtro filt = NORMAL;
	FILE *entrada = stdin;
	FILE *saida = stdout;
	float aten = 0.5;
	int delay = 1000;
	int i;

	/* verifica se tem o -i */
	opcao_entrada(argc, argv, &entrada);
	/* verifica se tem o -o */
	opcao_saida (argc, argv, &saida);

	/* confere se entrada para aten e delay*/
	for (i = 0; i < argc; i++) {
			if (strcmp(argv[i], "-l") == 0)
				aten = atof(argv[i+1]);
			if (strcmp(argv[i], "-t") == 0)
				delay = atoi(argv[i+1]);
	}

	/* se aten ou delay tiver um valor inadequado */
	if ((aten < 0) || (aten > 1.0)) {
		fprintf(stderr, "Valor de aten inadequado.\n");
		return (-1);
	}
	if (delay < 0) {
		fprintf(stderr, "Valor de delay inadequado.\n");
		return (-1);
	}


	ler_arquivo(&audio, entrada);

	echo(&audio, aten, delay);

	escreve_header(&audio, saida);

	escreve_amostra(&audio, filt, saida);

	libera_memoria(&audio);

	int fcloseall (void);

	return (0);
	
}