#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "struct.h"
#include "escrita.h"
#include "leitura.h"


int main(int argc, char **argv)
{
    int i;
    int j = 0;
    int filt = NORMAL;
    int n_arg = argc - 2;    // tira o -o e a saida
    FILE *saida = stdout;
    FILE *arquivos[n_arg];   // vetor para armazrnar os arquivos
    wav_t waves[n_arg];      // vetor para armazenar os waves

    /* confere o -o */
    opcao_saida (argc, argv, &saida);

    /* receber os arquivos de entrada */
    for (i = 1; i < n_arg; i++) {
        arquivos[j] = fopen(argv[i], "rb");
        j++;
    }

    /* ler os arquivos de entrada */ 
    for (i = 0; i < j; i++)
        ler_arquivo(&waves[i], arquivos[i]);
  
  	/* somar o numero de amostras e o tamanho que elas ocupam */
    for (i = 1; i < j; i++) {
        waves[0].c6.schunk2size += waves[i].c6.schunk2size;
        //waves[0].c7.a_data += waves[i].c7.a_data;
    }

    /* escreve o cabecalho */
    escreve_header(&waves[0], saida);

    /* escreve as amostras */
    for (i = 0; i < j; i++) {
        escreve_amostra(&waves[i], filt, saida);
        libera_memoria(&waves[i]);
    }

    int fcloseall (void);
    
    
    return (0);
}