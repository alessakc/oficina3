#ifndef __LEITURA__
#define __LEITURA__

void ler_arquivo(wav_t *audio, FILE *entrada);
void opcao_entrada (int argc, char *argv[], FILE **entrada);
void opcao_saida (int argc, char *argv[], FILE **saida);
void libera_memoria (wav_t *audio);

#endif