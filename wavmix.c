#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "struct.h"
#include "escrita.h"
#include "leitura.h"
#include "wavmix.h"

int main(int argc, char **argv)
{
    int i, k, maior;
    int j = 0;
    int filt = NORMAL;
    int n_arg = argc - 2;    // tira o -o e a saida
    FILE *saida = stdout;
    FILE *arquivos[n_arg];   // vetor para armazrnar os arquivos
    wav_t waves[n_arg];      // vetor para armazenar os waves

    /* confere o -o */
    opcao_saida (argc, argv, &saida);

    /* receber os arquivos de entrada */
    for (i = 1; i < n_arg; i++) {
        arquivos[j] = fopen(argv[i], "rb");
        j++;
    }

    /* ler os arquivos de entrada */ 
    for (i = 0; i < j; i++)
        ler_arquivo(&waves[i], arquivos[i]);

    /* acha o maior arquivo de entrada */
    maior = 0;
    for (i = 0; i < j; i++)
        if (waves[i].c7.a_data > waves[maior].c7.a_data)
            maior = i;

    /* escreve o cabecalho */
    escreve_header(&waves[maior], saida);

    /* multiplica as amostras */
    for (j = 1; j <= n_arg - 2; j++) {
        /* n total de amostras multiplicado pelo n de canais */
        k = (waves[j].c7.a_data)*(waves[j].c3.n_canais) + 1;
        /* soma as amostras de cada arquivo */
        for (i = 0; i < k; i++)
            waves[maior].c7.amostras[i] += waves[j].c7.amostras[i];
    }

    /* escreve as amostras */
    escreve_amostra(&waves[maior], filt, saida);

    for (i = 0; i < j; i++)
        libera_memoria(&waves[i]);

    int fcloseall (void);
    
    
    return (0);
}