#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"

	
int main (int argc, char *argv[]) {

	wav_t audio;
	int i;
	FILE *saida = stdout;
	FILE *entrada = stdin;
	
	/* confere se tem -i */
	opcao_entrada(argc, argv, &entrada);
	/* confere se tem - */
	opcao_saida(argc, argv, &saida);

	/* le o arquivo */
    ler_arquivo(&audio, entrada);
   		
	/* imprime as informacoes */
	fprintf(saida, "riff tag       : \"");
	for (i = 0; i < 4; i++)
		fprintf(saida, "%c", audio.c1.chunkID[i]);
	fprintf(saida, "\"\n");

	fprintf(saida, "riff size      : %d\n", audio.c1.chunksize);

	fprintf(saida, "wave tag       : \"");
	for (i = 0; i < 4; i++)
		fprintf(saida, "%c", audio.c1.format[i]);
	fprintf(saida, "\"\n");

	fprintf(saida, "form tag       : \"");
	for (i = 0; i < 4; i++)
		fprintf(saida, "%c", audio.c2.schunk1[i]);
	fprintf(saida, "\"\n");

	fprintf(saida, "fmt_size       : %d\n", audio.c2.schunk1size);
	fprintf(saida, "audio_format   : %hi\n", audio.c3.audiof);
	fprintf(saida, "num_channels   : %hi\n", audio.c3.n_canais);
	fprintf(saida, "sample_rate    : %d\n", audio.c4.srate);
	fprintf(saida, "byte_rate      : %d\n", audio.c4.brate);
	fprintf(saida, "block_align    : %hi\n", audio.c5.balign);
	fprintf(saida, "bits_per_sample: %hi\n", audio.c5.bsample);

	fprintf(saida, "data tag       : \"");
	for (i = 0; i < 4; i++)
		fprintf(saida, "%c", audio.c6.schunk2[i]);
	fprintf(saida, "\"\n");

	fprintf(saida, "data size      : %d\n", audio.c6.schunk2size);
	fprintf(saida, "samples/channel: %d\n", audio.c7.a_data);

	libera_memoria(&audio);

	int fcloseall (void);
	
	return (0);
	
}