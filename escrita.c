#include <stdio.h>
#include <stdlib.h>
#include "struct.h"
#include "escrita.h"

void escreve_amostra(wav_t *audio, int filt, FILE * saida) {

	int	k = (audio->c7.a_data)*(audio->c3.n_canais);
	int i;
		/* se for normal */
	if (!filt)
		fwrite (audio->c7.amostras, 2*audio->c7.a_data+1, 2, saida);
	
	/* se for reverso */
	if (filt)
		for (i = k; i >= 0; i--) 
			fwrite(&audio->c7.amostras[i], 1, 2, saida);

}


void escreve_header(wav_t *audio, FILE *saida) {

	fwrite (audio->c1.chunkID, 5, 4, saida);
	fwrite (&audio->c3.audiof, 2, 2, saida);
	fwrite (&audio->c4.srate, 2, 4, saida);
	fwrite (&audio->c5.balign, 2, 2, saida);
	fwrite (audio->c6.schunk2, 2, 4, saida);
	
}

