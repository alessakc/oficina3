#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "leitura.h"
#include "escrita.h"

void estereo(wav_t *audio, float k) {

	int i, j;
	short int diff;
	int t = (audio->c7.a_data)*(audio->c3.n_canais) + 1;

	j = 1;
	i = 0;
	while (i < t) {
		diff = audio->c7.amostras[i] - audio->c7.amostras[j];
		if ((audio->c7.amostras[i] + k*diff) > 32767)
			audio->c7.amostras[i] = 32767;
		if ((audio->c7.amostras[i] + k*diff) < -32767)
			audio->c7.amostras[i] = -32767;
		else	
			audio->c7.amostras[i] = audio->c7.amostras[i] + k*diff;
		if ((audio->c7.amostras[j] - k*diff) > 32767)
			audio->c7.amostras[j] = 32767;
		if ((audio->c7.amostras[j] + k*diff) < -32767)
			audio->c7.amostras[j] = -32767;
		else
			audio->c7.amostras[j] = audio->c7.amostras[j] - k*diff;
		i += audio->c3.n_canais;
		j += audio->c3.n_canais;
	}

}

int main (int argc, char *argv[]) {

	filtro filt = NORMAL;
	wav_t audio;
	FILE *entrada = stdin;
	FILE *saida = stdout;
	int i;
	float k = 1.0;

	/* verifica se tem o -i */
	opcao_entrada(argc, argv, &entrada);
	/* verifica se tem o -o */
	opcao_saida (argc, argv, &saida);

	/* confere se tem entrada de level*/
	for (i = 0; i < argc; i++) {
			if (strcmp(argv[i], "-l") == 0)
				k = atof(argv[i+1]);
	}

	/* se level tiver um valor inadequado */
	if ((k < 0) || (k > 10.0)) {
		fprintf(stderr, "Valor de k inadequado.\n");
		return (-1);
	}


	ler_arquivo(&audio, entrada);

	estereo(&audio, k);

	escreve_header(&audio, saida);

	escreve_amostra(&audio, filt, saida);	

	libera_memoria(&audio);

	int fcloseall (void);
	
	
	return (0);
}